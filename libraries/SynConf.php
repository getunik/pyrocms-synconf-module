<?php defined('BASEPATH') or exit('No direct script access allowed');

define('NATIVE_JSON_PRETTY_PRINT', false);

class SynConf
{
	private $ci;
	private $groupConfig;
	private $groupSummary;

	public function __construct($groupConfig = NULL)
	{
		$this->ci = &get_instance();
		$this->ci->load->database();

		$this->groupConfig = $groupConfig;
		if ($this->groupConfig !== NULL)
		{
			$this->groupSummary = $this->getSummary();
		}
	}

	public function dumpGroup($dumpCallback = NULL)
	{
		$dumpDir = $this->getPath();
		$this->ensureDirectory($dumpDir);

		if ($dumpCallback === NULL)
			$dumpCallback = array($this, 'getTableDataJson');

		$hashes = array();
		foreach ($this->groupConfig['tables'] as $tableName)
		{
			$json = $dumpCallback($tableName);
			$fileName = "$dumpDir/$tableName.json";

			file_put_contents($fileName , $json);
			$hashes[$tableName] = sha1($json);
		}

		file_put_contents("$dumpDir/summary.json", self::json_encode_pretty($hashes));
	}

	public function importGroup($importCallback = NULL)
	{
		$dumpDir = $this->getPath();

		if ($importCallback === NULL)
			$importCallback = array($this, 'importTable');

		foreach ($this->groupConfig['tables'] as $tableName)
		{
			$data = $this->getFileData($tableName);
			$importCallback($tableName, $data);
		}
	}

	public function importTable($tableName, $data)
	{
		if (!is_array($data))
			throw new Exception("Expecting table data to be an array ($tableName)");

		$prefix = $this->ci->db->dbprefix;
		$this->ci->db->set_dbprefix();

		try
		{
			if ($this->ci->db->trans_start() === FALSE)
				throw new Exception("Unable to start transaction");

			$this->ci->db->query("TRUNCATE TABLE `$tableName`");

			foreach ($data as $entry)
			{
				$this->ci->db->insert($tableName, $entry);
			}

			if (!$this->ci->db->trans_complete())
				throw new Exception("Transaction failed for table $tableName");

			$this->ci->db->set_dbprefix($prefix);
		}
		catch (Exception $e)
		{
			$this->ci->db->trans_rollback();
			$this->ci->db->set_dbprefix($prefix);
			throw $e;
		}
	}

	public function getFileData($tableName)
	{
		return json_decode($this->getFileDataJson($tableName));
	}

	public function getFileDataJson($tableName)
	{
		$dumpDir = $this->getPath($this->groupConfig['path']);
		$fileName = "$dumpDir/$tableName.json";

		return file_get_contents($fileName);
	}

	public function getTableData($tableName)
	{
		if (!preg_match('/^\w+$/', $tableName))
			throw new Exception("invalid table name '$tableName'");

		return $this->ci->db->query("SELECT * FROM `$tableName`")->result();
	}

	public function getTableDataJson($tableName)
	{
		return self::json_encode_pretty($this->getTableData($tableName));
	}

	public function getHashFromFile($tableName)
	{
		if ($this->groupSummary === NULL || !property_exists($this->groupSummary, $tableName))
			return '';

		return $this->groupSummary->{$tableName};
	}

	public function getHashFromData($tableName)
	{
		return sha1($this->getTableDataJson($tableName));
	}

	private function getSummary()
	{
		$dumpDir = $this->getPath($this->groupConfig['path']);
		if (!file_exists("$dumpDir/summary.json"))
			return NULL;

		$json = file_get_contents("$dumpDir/summary.json");
		return json_decode($json);
	}

	private function getPath()
	{
		$vars = array(
			'PYROROOT' => substr(FCPATH, -1) === '/' || substr(FCPATH, -1) === '\\' ? substr(FCPATH, 0, -1) : FCPATH,
		);

		return preg_replace_callback(
			'/\$\(([^)]*)\)/', // $(VARIABLE)
			function ($matches) use ($vars) {
				return array_key_exists($matches[1], $vars) ? $vars[$matches[1]] : '';
			},
			$this->groupConfig['path']
		);
	}

	private function ensureDirectory($pathName)
	{
		if (!is_dir($pathName))
			mkdir($pathName, 0775, true);
	}

	public static function json_encode_pretty($data)
	{
		if (NATIVE_JSON_PRETTY_PRINT)
		{
			return json_encode($data, JSON_PRETTY_PRINT);
		}
		else
		{
			return self::json_readable_encode($data);
		}
	}

	/*
	Modified version of code found at: http://svn.kd2.org/svn/misc/libs/tools/json_readable_encode.php

	json readable encode
	basically, encode an array (or object) as a json string, but with indentation
	so that i can be easily edited and read by a human

	THIS REQUIRES PHP 5.3+

	Copyleft (C) 2008-2011 BohwaZ <http://bohwaz.net/>

	Licensed under the GNU AGPLv3

	This software is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this software. If not, see <http://www.gnu.org/licenses/>.
	*/
	private static function json_readable_encode($in, $indent = 0, $indentStr = "    ", Closure $_escape = null)
	{
		if (is_null($_escape))
		{
			$_escape = function ($str)
			{
				return str_replace(
					array('\\', '"', "\n", "\r", "\b", "\f", "\t", '/', '\\\\u'),
					array('\\\\', '\\"', "\\n", "\\r", "\\b", "\\f", "\\t", '\\/', '\\u'),
					$str);
			};
		}

		$out = '';

		if (is_array($in) && !self::is_assoc($in))
		{
			$delimL = '[';
			$delimR = ']';
			foreach ($in as $item)
			{
				$out .= self::json_readable_encode_item($item, $indent, $indentStr, $_escape);
				$out .= ",\n";
			}
		}
		else
		{
			$delimL = '{';
			$delimR = '}';
			foreach ($in as $key=>$value)
			{
				$out .= str_repeat($indentStr, $indent + 1);
				$out .= "\"".$_escape((string)$key)."\": ";

				$out .= self::json_readable_encode_item($value, $indent, $indentStr, $_escape);

				$out .= ",\n";
			}
		}

		if (!empty($out))
		{
			$out = substr($out, 0, -2);
		}

		$out = str_repeat($indentStr, $indent) . $delimL . "\n" . $out;
		$out .= "\n" . str_repeat($indentStr, $indent) . $delimR;

		return $out;
	}

	private static function json_readable_encode_item($value, $indent = 0, $indentStr = "    ", Closure $_escape = null)
	{
		$out = '';

		if (is_object($value) || is_array($value))
		{
			$out .= self::json_readable_encode($value, $indent + 1, $indentStr, $_escape);
		}
		elseif (is_bool($value))
		{
			$out .= $value ? 'true' : 'false';
		}
		elseif (is_null($value))
		{
			$out .= 'null';
		}
		elseif (is_string($value))
		{
			$out = json_encode($value);
		}
		else
		{
			$out .= $value;
		}

		return $out;
	}

	private static function is_assoc($arr)
	{
		return array_keys($arr) !== range(0, count($arr) - 1);
	}
}
