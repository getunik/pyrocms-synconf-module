<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller
{
	private $tableGroups;
	private $remotes;

	public function __construct()
	{
		parent::__construct();

		$this->config->load('synconf/tables');
		$this->tableGroups = $this->config->item('tables');

		$this->config->load('synconf/remotes');
		$this->remotes = $this->config->item('remotes');

		$this->load->language('synconf_admin');

		$this->load->library('synconf/SynConf');
		Asset::add_path('synconf', $this->module_details['path'] . '/');
		$this->template->append_js("synconf::admin.js");
		$this->template->append_css("synconf::admin.css");
	}

	public function index()
	{
		$this->template->build('admin/index', array('tableGroups' => $this->tableGroups, 'remotes' => $this->remotes));
	}

	public function dump($groupKey)
	{
		role_or_die('synconf', 'dump', 'admin', 'Access Denied');
		$groupConfig = $this->getGroupConfig($groupKey);
		$synConf = new SynConf($groupConfig);

		$synConf->dumpGroup();

		$this->session->set_flashdata('success', 'Dump Successful');
		redirect('admin/synconf/status/' . $groupKey);
	}

	public function remoteDump($remoteKey, $groupKey)
	{
		role_or_die('synconf', 'fetch', 'admin', 'Access Denied');
		$groupConfig = $this->getGroupConfig($groupKey);
		$synConf = new SynConf($groupConfig);
		$that = $this;

		try
		{
			$synConf->dumpGroup(function ($tableName) use ($that, $remoteKey, $groupKey) {
				$data = $that->remoteRequest($remoteKey, 'tableData/' . $groupKey . '/' . $tableName);
				$data = ($data === NULL ? array() : $data);
				$json = SynConf::json_encode_pretty($data);
				return $json;
			});

			$this->session->set_flashdata('success', 'Dump Successful');
		}
		catch (Exception $e)
		{
			$this->session->set_flashdata('success', 'Dump Failed: ' . $e->getMessage());
		}

		redirect('admin/synconf/remotestatus/' . $remoteKey . '/' . $groupKey);
	}

	public function import($groupKey)
	{
		role_or_die('synconf', 'import', 'admin', 'Access Denied');
		$groupConfig = $this->getGroupConfig($groupKey);
		$synConf = new SynConf($groupConfig);

		try
		{
			$synConf->importGroup();
			$this->session->set_flashdata('success', 'Import Successful');
		}
		catch (Exception $e)
		{
			$this->session->set_flashdata('error', $e->getMessage());
		}

		redirect('admin/synconf/status/' . $groupKey);
	}

	public function remoteImport($remoteKey, $groupKey)
	{
		role_or_die('synconf', 'push', 'admin', 'Access Denied');
		$groupConfig = $this->getGroupConfig($groupKey);
		$synConf = new SynConf($groupConfig);
		$that = $this;

		try
		{
			$synConf->importGroup(function ($tableName, $data) use ($that, $remoteKey, $groupKey) {
				$result = $that->remoteRequest($remoteKey, 'tableData/' . $groupKey . '/' . $tableName, json_encode($data));
				var_dump($result);
				if ($result === NULL)
					throw new Exception("Remote import failed probably due to lacking SynConf permissions");
				if ($result->success !== true)
					throw new Exception("Remote import failed for table '$tableName' with message: {$result->error}");
			});

			$this->session->set_flashdata('success', 'Remote import Successful');
		}
		catch (Exception $e)
		{
			$this->session->set_flashdata('error', $e->getMessage());
		}

		redirect('admin/synconf/remotestatus/' . $remoteKey . '/' . $groupKey);
	}

	public function status($groupKey)
	{
		$groupConfig = $this->getGroupConfig($groupKey);
		$synConf = new SynConf($groupConfig);

		$tables = array();
		foreach ($groupConfig['tables'] as $tableName)
		{
			$fileHash = $synConf->getHashFromFile($tableName);
			$dataHash = $synConf->getHashFromData($tableName);
			$tables[] = array(
				'name' => $tableName,
				'fileHash' => $fileHash,
				'dataHash' => $dataHash,
				'ok' => $dataHash === $fileHash,
			);
		}

		$viewData = array(
			'remote' => false,
			'groupKey' => $groupKey,
			'groupConfig' => $groupConfig,
			'tables' => $tables,
		);

		$this->template->build('admin/status', $viewData);
	}

	public function remoteStatus($remoteKey, $groupKey)
	{
		$groupConfig = $this->getGroupConfig($groupKey);
		$remoteConfig = $this->getRemoteConfig($remoteKey);
		$synConf = new SynConf($groupConfig);

		$remoteStatus = $this->remoteRequest($remoteKey, 'status/' . $groupKey);

		$tables = array();
		foreach ($groupConfig['tables'] as $tableName)
		{
			$fileHash = $synConf->getHashFromFile($tableName);
			$dataHash = property_exists($remoteStatus, $tableName) ? $remoteStatus->{$tableName} : '';
			$tables[] = array(
				'name' => $tableName,
				'fileHash' => $fileHash,
				'dataHash' => $dataHash,
				'ok' => $dataHash === $fileHash,
			);
		}

		$viewData = array(
			'remote' => true,
			'remoteKey' => $remoteKey,
			'remoteConfig' => $remoteConfig,
			'groupKey' => $groupKey,
			'groupConfig' => $groupConfig,
			'tables' => $tables,
		);

		$this->template->build('admin/status', $viewData);
	}

	private function getGroupConfig($groupKey)
	{
		if (!array_key_exists($groupKey, $this->tableGroups))
		{
			show_error("Unknown table group '$groupKey'");
			exit;
		}

		return $this->tableGroups[$groupKey];
	}

	private function getRemoteConfig($remoteKey)
	{
		if (!array_key_exists($remoteKey, $this->remotes))
		{
			show_error("Unknown remote '$remoteKey'");
			exit;
		}

		return $this->remotes[$remoteKey];
	}

	private function remoteRequest($remoteKey, $method, $data = NULL)
	{
		$remoteConfig = $this->remotes[$remoteKey];

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
		if ($remoteConfig['auth'] !== 'none')
		{
			if ($remoteConfig['auth'] === 'basic')
			{
				curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			}
			elseif ($remoteConfig['auth'] === 'digest')
			{
				curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
			}

			curl_setopt($curl, CURLOPT_USERPWD, $remoteConfig['username'] . ':' . $remoteConfig['password']);
		}
		curl_setopt($curl, CURLOPT_URL, $remoteConfig['baseuri'] . '/synconf/api/' . $method);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

		if ($data !== NULL)
		{
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
			));
		}

		$res = curl_exec($curl);
		curl_close($curl);

		return json_decode($res);
	}
}
