<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Test extends Public_Controller
{
	public function __construct()
	{
		$this->load->database();
		$this->load->library('synconf/SynConf');
	}

	private function getJsonData()
	{
		$result = $this->db->query('select * from default_settings')->result();
		return SynConf::json_encode_pretty($result);
	}

	public function show()
	{
		$json = $this->getJsonData();

		var_dump($json);
		echo '<hr />';
		var_dump(json_decode($json));
	}

	public function compare()
	{
		$left = function($data) {
			return json_encode($data, JSON_PRETTY_PRINT);
		};
		$right = function($data) {
			return SynConf::json_encode_pretty($data);
		};

		$this->compareTable($left, $right, 'default_pages');
		$this->compareTable($left, $right, 'default_settings');
		$this->compareTable($left, $right, 'default_data_streams');
		$this->compareTable($left, $right, 'default_def_page_fields');
		$this->compareTable($left, $right, 'default_email_templates');
		$this->compareTable($left, $right, 'default_magazine_articles');
		$this->compareTable($left, $right, 'default_magazine_magazines');
		$this->compareTable($left, $right, 'default_variables');
		$this->compareTable($left, $right, 'default_widget_areas');
	}

	private function compareTable($left, $right, $table)
	{
		$result = $this->db->query('select * from ' . $table)->result();
		$l = $left($result);
		$ml = md5($l);
		$r = $right($result);
		$mr = md5($r);

		echo $table . "<br />";
		echo $ml . ' --- ' . $mr . "<br />";

		if ($ml !== $mr)
		{
			echo "<hr />";
			echo $l;
			echo "<hr />";
			echo $r;
			echo "<hr />";
		}
	}

	public function insert()
	{
		$this->load->database();

		$obj = new stdClass();
		$obj->key = 'inserted ' . date("His");
		$obj->value = 'this is inserted with codeigniter';

		$this->db->insert('synconf_test', $obj);
	}

	public function sha1()
	{
		echo sha1($this->getJsonData());
	}

	public function basicauth()
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_USERPWD, "rest:restitution");
		curl_setopt($curl, CURLOPT_URL, 'https://sandbox.pyro.localhost/synconf/api/status/config');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		$res = curl_exec($curl);
		curl_close($curl);

		var_dump(json_decode($res));
	}

	public function digestauth()
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
		curl_setopt($curl, CURLOPT_USERPWD, "rest:restitution");
		curl_setopt($curl, CURLOPT_URL, 'http://sandbox.pyro.localhost/synconf/api/status/config');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		$res = curl_exec($curl);
		curl_close($curl);

		var_dump(json_decode($res));
	}

	public function digest2()
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
		curl_setopt($curl, CURLOPT_USERPWD, "rest:restitution");
		curl_setopt($curl, CURLOPT_URL, 'http://sandbox.pyro.localhost/synconf/api/tabledata/config/synconf_test');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		$res = curl_exec($curl);
		curl_close($curl);

		var_dump(json_decode($res));
	}
}
