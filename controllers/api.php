<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api extends REST_Controller
{
	private $tableGroups;

	public function __construct()
	{
		parent::__construct();
		$this->config->load('synconf/tables');
		$this->load->library('synconf/SynConf');
		$this->tableGroups = $this->config->item('tables');
	}

	public function status_get($groupKey)
	{
		$groupConfig = $this->getGroupConfig($groupKey);
		$synConf = new SynConf($groupConfig);

		$result = new stdClass();
		foreach ($groupConfig['tables'] as $tableName) {
				$result->{$tableName} = $synConf->getHashFromData($tableName);
		}

		$this->response($result);
	}

	public function tableData_get($groupKey, $tableName)
	{
		$groupConfig = $this->getGroupConfig($groupKey);
		$synConf = new SynConf($groupConfig);

		if ($groupConfig['allowRemoteDump'] !== true || array_search($tableName, $groupConfig['tables']) === FALSE)
			return;

		$data = $synConf->getTableData($tableName);
		$this->response($data);
	}

	public function tableData_post($groupKey, $tableName)
	{
		$groupConfig = $this->getGroupConfig($groupKey);
		$synConf = new SynConf($groupConfig);

		if ($groupConfig['allowRemoteImport'] !== true || array_search($tableName, $groupConfig['tables']) === FALSE)
			return;

		try
		{
			$synConf->importTable($tableName, $this->request->body);
			$this->response(array('success' => true));
		}
		catch (Exception $e)
		{
			$this->response(array(
				'success' => false,
				'error' => $e->getMessage(),
			));
		}
	}

	private function getGroupConfig($groupKey)
	{
		if (!array_key_exists($groupKey, $this->tableGroups))
		{
			show_error("Unknown table group '$groupKey'");
			exit;
		}

		return $this->tableGroups[$groupKey];
	}
}
