<?php defined('BASEPATH') OR exit('No direct script access allowed');

$name = 'synconf';

$lang["$name:role_dump"] = 'Dump Tables';
$lang["$name:role_import"]	= 'Import Tables';
$lang["$name:role_fetch"]	= 'Fetch Tables';
$lang["$name:role_push"]	= 'Push Tables';
