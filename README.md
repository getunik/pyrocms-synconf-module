# SynConf module for PyroCMS

Provides a backend interface to dump and import table data in JSON format to store configuration data under source control

- version - 0.4.0
- Authors - Lukas Angerer - lukas.angerer@getunik.com
- Website - [getunikAG](http://www.getunik.com/)

## Disclaimer
The module was designed as a way to dump and import small groups of tables containing configuration data (as opposed to actual content tables) and keeping track of changes using a SCM system like Git. It can potentially be used for other things as well and in the future it will most likely get some more features to support other use cases, but right now it's intended useage scenario is very limited. **In any case, if you're using this module, keep regular full database backups in case things go awry.**

## Performance
At this point, the module is absolutely **not** optimized or built for speed in the first place. It is not smart at all and just dumps all data of a table and truncates the whole table and inserts entries one by one for imports. I'm assuming that perfromance for any significant amount of data will be horrible at this point - agiain; the module was not designed to do that.

## Installation

The basics should work out of the box as sonn as you've installed the module in the backend. If you want to use the remotes functionality, there are some tweaks you'll have to make to your PyroCMS setup:

### REST_Controller bugfix
For all its remote functionality, the SynConf module relies on the not-yet-officially-supported REST_Controller. Unfortunately, the controller contains a bug in version 2.2.3 of PyroCMS which (depending on your PHP version and configuration) might break everything. There is an issue with a matching pull request in the official PyroCMS repository https://github.com/pyrocms/pyrocms/issues/3118 . You can simply apply the patch from this pull request to your REST_Controller.

### Configuring REST
For the REST functionality used for remote fetching and pushing, there is a configuration file _PROJECTROOT/system/cms/config/rest.php_. The default configuration doesn't really work out of the box, so here are the tweaks you have to make:

* configure `rest_auth` to your desire; I would recommend digest authentication
* define at least one login / password pair in the `rest_valid_logins` - make sure to pick a secure password; you don't want just anybody remotely dumping and importing data!
* set `rest_enable_keys` to `false`
* set `rest_enable_logging` to `false`

## Basics
You can find the backend interface for SynConf in the _Data_ menu. It gives you an overview over your table groups. A table group is a set of tables that logically belong together - the default configuration comes with a _Configs_ group that contains all the _pure_ configuration tables. Clicking on the _Show_ button next to the group name gets you to the status screen where you see all the tables in the group and their _file_ and _database_ hashes. To get some files, you can click the _Dump_ button. This will dump all content of the listed tables into their configured location as JSON files (one file per table and one summary file). The hashes are calculated over a full export of the contents, so even looking at the status screen will essentially dump everything - it just won't store it yet.

## Remotes
Remotes are something of an experimental feature at this point. The idea is simple: allow dump and import operations from and to remote PyroCMS instances. The purpose of this is of course synchronizing configurations across environments (local, development, staging, ...). For clarity purposes, the dump and import operations in the context of a remote location are referred to as _fetch_ and _push_ instead.

You can configure as many remotes as you want and as soon as you have one, it will appear on the overview page as a radio button. Selecting a remote and clicking the _Show_ button will then bring you to the remote status screen where you can initiate _Fetch_ and _Push_ operations similar to the local context. The main difference being that the data is retrieved and sent through the SynConf REST API webservice.

There are no remotes by default, but the module's config directory contains an example configuration file (remote-sample.php) that looks something like this:

```
<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['remotes'] = array(
	'test' => array(
		'name' => 'Sample Remote',
		'username' => 'myusername',
		'password' => 'mypassword',
		'auth' => 'digest', // allowed: 'none', 'basic', 'digest'
		'baseuri' => 'https://myremote.net', // it is always a good idea to use https
	),
);
```

The meaning of the individual settings should be pretty obvious. Make sure that your `baseuri` does not have a trailing slash since this would not work at this point. Just edit and rename the file to `remotes.php`

An important thing to note is that your remote's tables configuration **should match** your local tables configuration; otherwise you might run into problems.

## Table Group Configuration
To modify and/or extend the default table groups, use the following sample as a template:

```
array(
    'name' => 'Test', // the name of the group as it appears in the UI
    'hidden' => false, // set this to true to hide this group in the UI (it can still be dumped / imported by calling the corresponding controller actions 'manually')
    'allowRemoteDump' => true, // set this to false to prevent remote dumping (fetching) of this group
    'allowRemoteImport' => true, // set this to false to prevent remote importing (pushing) of this group
    'path' => '$(PYROROOT)/synconf/test', // path to the directory where this group's dumped files will be stored - the variable $(PYROROOT) contains the path to your PyroCMS root (location of the index.php)
    'tables' => array( // this is simply a list of all the (full) table names that are part of this group
        'some_table_name',
    ),
)
```

## Security
If you are really concerned about security, then the best solution would be to simply not use remotes and disable the REST API with the REST config. If you are just _mildly_ concerned, then here are some security tips:

### Dumped Files in the Webroot
If your dump file locations are located under the PyroCMS webroot - which is the case by default - then your dump files are accessible remotely. You probably want to avoid that. You can either move the dump file location somewhere outside of the webroot or simply add a `.htaccess` file with the following content to prevent remote access:

```
deny from all
```
