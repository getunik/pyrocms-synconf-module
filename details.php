<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_SynConf extends Module
{
	public $version = '0.4.0';

	private $module_name;
	private $streams_data;
	private $app_config;
	private $langs;

	public function __construct()
	{
		parent::__construct();
		$this->module_name = strtolower(str_replace('Module_', '', get_class()));
	}

	public function info()
	{
		$config = array(
			'name' => array(
				'en' => 'SynConf'
			),
			'description' => array(
				'en' => 'Syncing Configs'
			),
			'backend' => true,
			'frontend' => false,
			'menu' => 'data',
			'roles' => array('dump', 'import', 'fetch', 'push'),
		);

		return $config;
	}

	public function install()
	{
		return true;
	}

	public function upgrade($old_version)
	{
		return true;
	}

	public function uninstall()
	{
		return true;
	}
}
