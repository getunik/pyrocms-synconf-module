
$(document).ready(function () {
	$('[data-confirm]').click(function (e) {
		var message = $(this).data('confirm');
		if (!confirm(message)) {
			e.preventDefault();
		}
	});

	$('.show-link').each(function () {
		$(this).data('baseurl', $(this).attr('href'));
	});

	$('[name=remote]').change(function (e) {
		var remote = $(this).val();
		var baseUrl = $('table[data-baseurl]').data('baseurl');

		$('.show-link').each(function () {
			var link = $(this);
			var groupName = link.data('group');

			if (remote === '_local_') {
				link.attr('href', baseUrl + '/status/' + groupName);
			} else {
				link.attr('href', baseUrl + '/remotestatus/' + remote + '/' + groupName);
			}
		});
	});

	$('#_local_').click();
});
