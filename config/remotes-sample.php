<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['remotes'] = array(
	'test' => array(
		'name' => 'Sample Remote',
		'username' => 'myusername',
		'password' => 'mypassword',
		'auth' => 'digest', // allowed: 'none', 'basic', 'digest'
		'baseuri' => 'https://myremote.net/', // it is always a good idea to use https
	),
);
