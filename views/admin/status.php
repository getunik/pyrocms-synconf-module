<section class="title">
	<h4>
		<?php echo sprintf(lang('status:title_' . ($remote ? 'remote' : 'local')), $groupConfig['name'], isset($remoteConfig) ? $remoteConfig['name'] : ''); ?>
	</h4>
</section>
<section class="item status">
	<div class="padding-box">
		<table>
			<tbody>
				<tr>
					<th><?php echo lang('lbl_table_name'); ?></th>
					<th><?php echo lang('lbl_file_hash'); ?></th>
					<th><?php echo lang('lbl_data_hash'); ?></th>
					<th><?php echo lang('lbl_ok'); ?></th>
				</tr>
				<?php foreach ($tables as $tableData) { ?>
					<tr class="<?php echo ($tableData['ok'] ? 'ok' : 'nok'); ?>">
						<td><?php echo $tableData['name']; ?></td>
						<td><?php echo $tableData['fileHash']; ?></td>
						<td><?php echo $tableData['dataHash']; ?></td>
						<td><?php echo ($tableData['ok'] ? lang('status_ok') : lang('status_nok')) ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>

		<div class="buttons">
			<?php if ($remote) { ?>
				<?php if (group_has_role('synconf', 'fetch')) { ?>
					<a class="button" href="<?php echo site_url('admin/synconf/remotedump/' . $remoteKey . '/' . $groupKey) ?>"><?php echo lang('btn_fetch'); ?></a>
				<?php } if (group_has_role('synconf', 'push')) { ?>
					<a class="button" href="<?php echo site_url('admin/synconf/remoteimport/' . $remoteKey . '/' . $groupKey) ?>" data-confirm="<?php echo lang('confirm_import'); ?>"><?php echo lang('btn_push'); ?></a>
				<?php } ?>
			<?php } else { ?>
				<?php if (group_has_role('synconf', 'dump')) { ?>
					<a class="button" href="<?php echo site_url('admin/synconf/dump/' . $groupKey) ?>"><?php echo lang('btn_dump'); ?></a>
				<?php } if (group_has_role('synconf', 'import')) { ?>
					<a class="button" href="<?php echo site_url('admin/synconf/import/' . $groupKey) ?>" data-confirm="<?php echo lang('confirm_import'); ?>"><?php echo lang('btn_import'); ?></a>
				<?php } ?>
			<?php } ?>
			<a class="button" href="<?php echo site_url('admin/synconf/') ?>"><?php echo lang('btn_back'); ?></a>
		</div>
	</div>
</section>
