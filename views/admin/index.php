
<section class="title">
	<h4><?php echo lang('index:title'); ?></h4>
</section>
<section class="item">
	<div class="padding-box">
		<div class="remotes">
			<input id="_local_" type="radio" name="remote" value="_local_" />
			<label for="_local_"><?php echo lang('local_remote'); ?></label>
			<?php foreach ($remotes as $key => $remoteConfig) { ?>
				<input id="<?php echo $key ?>" type="radio" name="remote" value="<?php echo $key ?>" />
				<label for="<?php echo $key ?>"><?php echo $remoteConfig['name'] ?></label>
			<?php } ?>
		</div>

		<table data-baseurl="<?php echo site_url('admin/synconf'); ?>">
			<thead>
				<tr>
					<th><?php echo lang('lbl_group'); ?></th>
					<th width="10%">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($tableGroups as $group => $groupConfig) {
					if ($groupConfig['hidden'] === true)
						continue;
				 ?>
					<tr class="group name-<?php echo $group; ?>">
						<td><?php echo $groupConfig['name']; ?></td>
						<td>
							<a class="show-link button" href="#" data-group="<?php echo $group; ?>"><?php echo lang('btn_show'); ?></a>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</section>
